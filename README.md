Downloads: https://bitbucket.org/Bigmacbook/yeeoverlay/downloads

**** Release Notes ****

V003:

+ Added support for audio
    - Select from a random sound file for each animation
    - enable/disable sounds for each overlay or all overlays
    - Known Supported Audio files: mp3,wav,m4a,ogg (chrome)


V002:

+ Added new animations (slide-down, fade)


V001:

+ Initial Release